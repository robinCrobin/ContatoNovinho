//
//  NovoContatoViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato)
    func editarContato()
}

class ContatoViewController: UIViewController {
    @IBOutlet weak var nomeField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var enderecoField: UITextField!
    @IBOutlet weak var numeroField: UITextField!
    
    public var contato: Contato?
    public var delegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nomeField.text = contato?.nome
        numeroField.text = contato?.telefone
        emailField.text = contato?.email
        enderecoField.text = contato?.endereco
        
        if contato == nil {
            title = "Novo Contato"
        } else {
            title = "Editar Contato"
        }
    }
    
    @IBAction func salvarContato(_ sender: Any) {
        if contato == nil{
            let contato = Contato(nome: nomeField?.text ?? "",
                                  email: emailField?.text ?? "",
                                  endereco: enderecoField?.text ?? "",
                                  telefone: numeroField?.text ?? "")
            
            delegate?.salvarNovoContato(contato: contato)
            
        } else {
            contato?.nome = nomeField?.text ?? ""
            contato?.telefone = numeroField?.text ?? ""
            contato?.email = emailField?.text ?? ""
            contato?.endereco = enderecoField?.text ?? ""
            
            delegate?.editarContato()
        }
        navigationController?.popViewController(animated: true)
    }
    
    

}
